#!/usr/bin/env node
const fs = require('fs')
const yargs = require('yargs')
const YAML = require('js-yaml')
const path = require('path')
const os = require('os')

const args = yargs
  .option('config', { describe: 'path to .coboxseederrc file', default: path.join(os.homedir(), '.coboxseederrc') })
  .middleware((argv) => Object.assign(argv, config.load(argv.config)))
  .commandDir('bin')
  .demandCommand()
  .alias('h', 'help')
  .alias('v', 'version')
  .help()

if (require.main === module) return args
  .usage('Usage: $0 <command> [options]')
  .argv

module.exports = args
