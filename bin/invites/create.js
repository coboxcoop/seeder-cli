const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const chalk = require('chalk')
const pick = require('lodash.pick')
const print = require('pretty-print')
const options = require('../lib/options')
const { handleAjaxErrors, getDefaultOpts } = require('../lib/util')
const { assign } = Object

exports.command = 'create <friends-key> [options]'
exports.description = 'create an invite code for an additional admin'
exports.builder = assign({}, getDefaultOpts(), pick(options, ['friendsKey']))
exports.handler = create

async function create (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const publicKey = argv.friendsKey

  try {
    let invite = await client.post(['invites'], { publicKey })
    print({ code: invite.content.code })
  } catch (err) {
    return handleAjaxErrors(err)
  }
}
