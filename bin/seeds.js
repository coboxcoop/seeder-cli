exports.command = "seeds <command>"
exports.desc = "seed your peers' folders"
exports.builder = function (yargs) {
  return yargs
    .commandDir('seeds')
    .demandCommand()
    .help()
}
exports.handler = function (argv) {}
