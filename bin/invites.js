exports.command = "invites <command>"
exports.desc = `manage invites for adding admins to your seeder's`
exports.builder = function (yargs) {
  return yargs
    .commandDir('invites')
    .demandCommand()
    .help()
}
exports.handler = function (argv) {}
