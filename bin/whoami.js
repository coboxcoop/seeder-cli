const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const print = require('pretty-print')
const chalk = require('chalk')
const { handleAjaxErrors, getDefaultOpts }= require('./lib/util')

exports.command = 'whoami'
exports.desc = `display your seeder's profile`
exports.builder = getDefaultOpts()
exports.handler = async function (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    var profile = await client.get('/profile')
    print(profile)
  } catch (err) {
    return handleAjaxErrors(err)
  }
}
