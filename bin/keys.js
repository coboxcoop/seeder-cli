exports.command = 'keys <command>'
exports.desc = `manage your seeder's keys`
exports.builder = function (yargs) {
  return yargs
    .commandDir('keys')
    .demandCommand()
    .help()
}
exports.handler = function (argv) {}
