const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const chalk = require('chalk')
const pick = require('lodash.pick')
const options = require('../lib/options')
const print = require('pretty-print')
const { handleAjaxErrors, getDefaultOpts } = require('../lib/util')
const { assign } = Object

exports.command = ['up [options]', 'add']
exports.description = 'start seeding all current seeds, or add a seed'
exports.builder = assign({}, getDefaultOpts(), pick(options, ['name', 'address']))
exports.handler = up

async function up (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    address = argv.address

  try {
    let replicators = await client.get('replicators')

    if (!(name || address)) {
      if (!replicators.length) return console.log(`! ${chalk.bold(`you have no existing seeds`)}, create one with\n${chalk.hex('#ACFFAC')(`seeds up --name <name> --address <address>`)}`)
      replicators = await client.post(['replicators', 'connections'])
      replicators.forEach(printReplicator)
    } else {
      let replicator = replicators.find((r) => r.name === name || r.address === address)
      if (replicator) return await onceReplicator(replicator)
      if (!address) return console.log(`! ${chalk.bold(`you must provide an address`)}`)
      replicator = await client.post('replicators', {}, { name, address })
      await onceReplicator(replicator)
    }
  } catch (err) {
    return handleAjaxErrors(err)
  }

  async function onceReplicator (replicator) {
    replicator = await client.post(['replicators', replicator.address, 'connections'], {}, replicator)
    print(replicator)
  }
}

function printReplicator (replicator) {
  console.log()
  print(replicator)
  console.log()
}
