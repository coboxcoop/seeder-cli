const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const print = require('pretty-print')
const options = require('../lib/options')
const { handleAjaxErrors, getDefaultOpts } = require('../lib/util')
const { assign } = Object

exports.command = 'remove <name|address>'
exports.desc = 'remove a seed permanently from disk'
exports.builder = assign({}, getDefaultOpts(), pick(options, ['name', 'address']))
exports.handler = remove

async function remove (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    address = argv.address

  try {
    let replicators = await client.get('replicators')
    let replicator = replicators.find((replicator) => replicator.name === name || replicator.address === address)
    if (replicator) return await onceSpace(replicator)
    else return console.log(`! ${chalk.bold(`seed doesn't exist`)}`)
  } catch (err) {
    return handleAjaxErrors(err)
  }

  async function onceSpace (replicator) {
    await Promise.all([
      client.delete(['replicators', replicator.address], {}, replicator)
    ])
    console.log(`${chalk.bold(`Seed removed successfully.`)}`)
    print(replicator)
  }
}
