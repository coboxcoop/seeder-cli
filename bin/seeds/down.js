const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const print = require('pretty-print')
const chalk = require('chalk')
const pick = require('lodash.pick')
const options = require('../lib/options')
const { handleAjaxErrors, getDefaultOpts } = require('../lib/util')
const { assign } = Object

exports.command = 'down [options]'
exports.description = 'stop seeding all current seeds or a single seed'
exports.builder = assign({}, getDefaultOpts(), pick(options, ['name', 'address']))
exports.handler = up

async function up (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  const name = argv.name,
    address = argv.address

  try {
    let replicators = await client.get('replicators')

    if (!name && !address) {
      if (!replicators.length) return console.log(`! ${chalk.bold(`you have no existing seeds`)}, create one with\n${chalk.hex('#ACFFAC')(`seeds up --name <name> --address <address>`)}`)
      replicators = await client.delete(['replicators', 'connections'])
      replicators.forEach(printReplicator)
    } else {
      let replicator = replicators.find((replicator) => (
        replicator.name === name || replicator.address === address)
      )

      if (replicator) {
        replicator = await client.delete(['replicators', replicator.address, 'connections'], {}, replicator)
        print(replicator)
      } else {
        return console.log(`! ${chalk.bold(`seed doesn't exist`)}`)
      }
    }
  } catch (err) {
    return handleAjaxErrors(err)
  }
}

function printReplicator (replicator) {
  console.log()
  print(replicator)
  console.log()
}
