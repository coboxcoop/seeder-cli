const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const print = require('pretty-print')
const { handleAjaxErrors, getDefaultOpts } = require('../lib/util')
const { assign } = Object

exports.command = 'ls'
exports.desc = "list your seeder's seeds"
exports.builder = getDefaultOpts()
exports.handler = ls

async function ls (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    let replicators = await client.get('replicators')
    replicators.forEach(printReplicator)
  } catch (err) {
    return handleAjaxErrors(err)
  }
}

function printReplicator (replicator) {
  console.log()
  print(replicator)
  console.log()
}
