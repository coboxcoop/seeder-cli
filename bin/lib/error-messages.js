const ISSUE_URL = 'http://gitlab.com/coboxcoop/seeder/issues'

const ECONNABORTED = (err) => {
  return `cobox-seeder is running but failed to respond.

try restarting your seeder with \'cobox-seeder stop\', then \'cobox-seeder start\'.

if that fails, please open an issue at ${ISSUE_URL} and provide the following error stack with steps to reproduce:

${JSON.stringify(err.toJSON(), null, 2)}`
}

const ECONNREFUSED = (err) => {
  return 'cobox-seeder is not running on this port, either specify the correct port or start the app using \'cobox-seeder start\''
}

const UNHANDLED_CLI_ERROR = (err) => {
    return `there has been an unexpected command line error... please open an issue at ${ISSUE_URL} and provide the following error stack with steps to reproduce

${JSON.stringify(err.toJSON(), null, 2)}`
}

const UNHANDLED_SERVER_ERROR = (err) => {
  return `there has been an unexpected server error... please email bugs@cobox.cloud with main.log

${JSON.stringify(err.toJSON(), null, 2)}`
}

module.exports = {
  ECONNABORTED,
  ECONNREFUSED,
  UNHANDLED_CLI_ERROR,
  UNHANDLED_SERVER_ERROR
}
