const os = require('os')

module.exports = {
  port: {
    alias: 'p',
    number: true,
    describe: 'seeder server port',
    default: 9111
  },
  address: {
    description: 'address of the folder',
    type: 'string',
    alias: 'f',
  },
  key: {
    description: 'encryption key of the folder',
    type: 'string',
    alias: 'e',
  },
  name: {
    description: 'name of the folder',
    type: 'string',
    alias: 'n'
  },
  code: {
    description: 'an invite code',
    type: 'string',
    alias: 'c',
  },
  friendsKey: {
    description: 'key of a friend',
    type: 'string',
    alias: 'k',
  },
  location: {
    type: 'string',
    describe: 'e.g. /path/to/backup.cobox',
    default: os.homedir()
  },
  raw: {
    type: 'boolean',
    describe: 'output raw data to stdout',
    default: false
  },
  json: {
    description: 'format type json',
    type: 'string'
  }
}
