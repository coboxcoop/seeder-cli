const constants = require('@coboxcoop/constants')
const options = require('./options')
const chalk = require('chalk')
const DEFAULT_COLOUR = '#EEE1FF'

const {
  ECONNABORTED,
  ECONNREFUSED,
  UNHANDLED_CLI_ERROR,
  UNHANDLED_SERVER_ERROR
} = require('./error-messages')

function onSuccess (msgs) {
  msgs = Array.isArray(msgs) ? msgs : [msgs]
  for (const msg of msgs) console.log(chalk.hex(DEFAULT_COLOUR)(msg, '\n'))
}

function onError (errs) {
  errs = Array.isArray(errs) ? errs : [errs]
  for (const err of errs) console.error(chalk.red(err), '\n')
}

function getDefaultOpts () {
  return Object
    .keys(constants.seederDefaults)
    .filter((k) => options[k])
    .reduce((acc, key) => { acc[key] = options[key]; return acc}, {})
}

function handleAjaxErrors (err) {
  if (err.response) {
    // the request was made and the server responded with a status code that is outside of the range 2xx
    var errors = err.response.data.errors
    // something wierd occurred server side
    if (!Array.isArray(errors)) return console.log(UNHANDLED_SERVER_ERROR(err))
    errors.forEach((err) => process.stderr.write(err.msg))
  } else if (err.request) {
    // the request was made but no response received, could have been a timeout, or the wrong port was specified
    if (err.code === 'ECONNREFUSED') return console.log(ECONNREFUSED(err))
    else if (err.code === 'ECONNABORTED') return console.log(ECONNABORTED(err))
    // TODO: not correctly handling other errors at the moment
    else return console.log(ECONNREFUSED(err))
  } else {
    // something happened when generating the request that caused an error
    return console.log(UNHANDLED_CLI_ERROR(err))
  }
}

module.exports = {
  DEFAULT_COLOUR,
  onSuccess,
  onError,
  getDefaultOpts,
  handleAjaxErrors
}
