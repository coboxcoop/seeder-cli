const fs = require('fs')
const path = require('path')
const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const pick = require('lodash.pick')
const chalk = require('chalk')
const options = require('../lib/options')
const { handleAjaxErrors, DEFAULT_COLOUR, getDefaultOpts } = require('../lib/util')
const { assign } = Object

exports.command = 'export'
exports.desc = "export your seeder's secrets"
exports.builder = assign({}, getDefaultOpts(), pick(options, ['location', 'raw']))
exports.handler = async function (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })

  try {
    var data = await client.get('keys')
    if (argv.raw) process.stdout.write(JSON.stringify(data))
    var location = path.join(argv.location, 'cobox-seeder.backup')
    var stream = fs.createWriteStream(location)
    stream.write(JSON.stringify(data))
    console.log(`! export written to ${chalk.hex(DEFAULT_COLOUR)(location)}`)
  } catch (err) {
    return handleAjaxErrors(err)
  }
}
